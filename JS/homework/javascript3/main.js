let result = 0, cheatCode = '1';
let prevOperation;

do {
    let num1;
    do {
        num1 = prompt('Number 1'); // "123"
    } while (isNaN(+num1) || num1 === null);

    let userInputOperation;
    let isValidOperation;
    let canUseCheatCode;
    let isCheatCode;
    do {
        userInputOperation = prompt('Operation:'); // "1"

        isValidOperation = userInputOperation === '*'
            || userInputOperation === '/'
            || userInputOperation === '+'
            || userInputOperation === '-';
        canUseCheatCode = Boolean(prevOperation);
        isCheatCode = userInputOperation === cheatCode;
    } while (!(isValidOperation || (canUseCheatCode && isCheatCode)));

    let operation;
    if(userInputOperation === cheatCode) {
        operation = prevOperation;
    }
    else {
        operation = userInputOperation;
    }

    let num2;
    do {
        num2 = prompt('Number 2'); // "2"
    } while (isNaN(+num2) || num2 === null);

    switch (operation) {
        case '*':
            result = num1 * num2;
            break;
        case '/':
            result = num1 / num2;
            break;
        case '+':
            result = +num1 + +num2;
            break;
        case '-':
            result = num1 - num2;
            break;
    }

    prevOperation = operation;
} while (confirm("Result: " + result + ".\nEshe raz?"));