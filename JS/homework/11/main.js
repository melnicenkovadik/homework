let buttons = document.querySelectorAll('.btn');

document.addEventListener('keypress', (event) => {
    buttonChange(event)
});

function buttonChange(event) {
    buttons.forEach(item => {
        item.style.background = 'black';
        if (item.innerText.toLowerCase() === event.key.toLowerCase()) {
            console.log(item);
            item.style.background = 'blue';
        }
    })
}