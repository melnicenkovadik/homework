// && - И   (если все правдивые - последний, иначе - первый ложный
// || – ИЛИ (если все ложные - последний, иначе - первый правдивый
// !  – НЕ  (boolean)

// falsy: false, null, undefined, 0, '', NaN

1 && 2 && 3 && "" && "A"
false || null || undefined || 0 || '' || NaN


false || 3 && "a" || "10" && 8 && true


/* ЗАДАНИЕ - 1
* Пользователь вводит в модальное
* окно(prompt) любое число. Нужно вывести на экран "Четное"
* или "Не четное" в зависимости от того четное число или нет.
*/

// const num = +prompt("Num:");
// if (num % 2) {
//   alert("Nechetnoe");
// } else {
//   alert("Chetnoe");
// }

// let result = (uslovie) ? (chtoto) : (drugoe_chtoto);
// const num = +prompt("Num:");
// alert(num % 2 ? "Nechetnoe" : "Chetnoe");


// Опросник на 5 вопросов (плохо(0-3), хорошо(4), отлично(5))
// let ans1 = +prompt('1+1=', '2');
// let ans2 = +prompt('1+3=', '4');
// let ans3 = +prompt('1+4=', '5');
// let ans4 = +prompt('1+5=', '6');
// let ans5 = +prompt('1+6=', '7');
// let correctAnswers =  (ans1 === 2)
//                     + (ans2 === 4)
//                     + (ans3 === 5)
//                     + (ans4 === 6)
//                     + (ans5 === 7);
//
// //...
// if (correctAnswers === 5) {
//   alert("Excellent");
// } else if (correctAnswers === 4) {
//   alert('Good');
// } else {
//   alert('Bad');
// }


/*
* Спросить пользователя на каком языке он хочет увидеть список дней недели.
* Пользователь может ввести  - ru, en, ukr.
* По умолчанию ru
* Вывести на экран список дней недели на выбранном языке.
* */
// const lang = prompt("Language: ", "ru");
// const listRu = '<ul><li>Пн</li><li>Вт</li><li>Ср</li><li>Чт</li><li>Пт</li><li>Сб</li><li>Вс</li></ul>'
// const listUkr = '<ul><li>Пон</li><li>Вт</li><li>Сер</li><li>Чт</li><li>Пт</li><li>Сб</li><li>Нд</li></ul>'
// const listEn = '<ul><li>Mon</li><li>Tue</li><li>Wed</li><li>Thu</li><li>Fri</li><li>Sat</li><li>Sun</li></ul>'

// if (lang === 'en') {
//   document.write(listEn);
// }
// else if (lang === 'ukr') {
//   document.write(listUkr);
// }
// else {
//   document.write(listRu);
// }

// switch(lang) {
//   case 'ukr': {
//     document.write(listUkr);
//     break;
//   }
//   case 'en': {
//     document.write(listEn);
//     break;
//   }
//   default: {
//     document.write(listRu);
//   }
// }




/*
* Пользователь вводит 3 числа.
* Нужно вывести на экран собщение
* с максимальным числом из введенных
* */
// let a = +prompt('A:');
// let b = +prompt('B:');
// let c = +prompt('C:');
//
// if(a > b && a > c) {
//   alert(a);
// }
// else if(b > c) {
//   alert(b);
// }
// else {
//   alert(c);
// }
//



// вывести день недели по порядковому номеру, вывести: будний, выходной


//
// let num = +prompt("Порядковый номер дня недели: ");
//
// if(isNaN(num) || num < 1 || num > 7) {
//   alert('Not a day of week');
// }
// else if(num <= 5) {
//   alert('Будний')
// }
// else {
//   alert("Выходной")
// }

// switch(num) {
//   case 1:
//   case 2:
//   case 3:
//   case 4:
//   case 5:
//     alert('Будний');
//     break;
//   case 6:
//   case 7:
//     alert("Выходной");
//     break;
//   default:
//     alert('Not a day of week');
// }

