// var, let, const difference
// 0) const, let - pretty much the same
// 1) const - cannot be modified compared to let
// 2) const, let - cannot be redeclared
// let a = 1;
// let a = 2; // Exception (Error)
// 3) const, let - block scoped ({})
// 4) var - hoists (всплывает) (hoisting)
// 5) var - functional scoped


// var v = 2;
// let l = 1;
//
// if(true) {
//   if (true) {
//     if (true) {
//       let l = 4;
//     }
//     console.log(l);
//   }
// }
// console.log(v);

// f();
// function f() {
//   if(true) {
//     console.log(a);
//     var v = 2;
//   }
//   let a = 5;
// }


// pure function
// function sum(num1, num2) {
//   return num1 + num2;
// }

// let result = sum(2, 3);
// console.log(result);


// function isEven(n) {
//   return n % 2 === 0;
// }

// console.log(isEven(5));
// console.log(isEven(6));


//
// function f(start, end, delitel) {
//   for(let num = start; num <= end; num++) {
//     if(num % delitel === 0) {
//       return true;
//     }
//     else {
//       return false;
//     }
//   }
// }

// console.log(f(1, 10000000000, 1000000000)); // true
// console.log(f(70, 80, 25)); // true
// console.log(f(10, 12, 15)); // false


function sum(n) {
    if(n === 0) {
        return 0;
    }
    return n + sum(n-1);
}


console.log(sum(5)); // 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10
// FIFO // first in first out – QUEUE (ochered)
// LIFO // last in first out – STACK (stack)


