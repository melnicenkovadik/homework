let a = 3;
let b = a;
b = 4;
// console.log(a, b);

let obj = {
    age: 13,
    "last name": "Last name123245",
    a: 1,
    b: "basbs",
    c: true,
    d: null,
    e: undefined,
    f: NaN,
    g: function() {}
};
obj.age = 14;
// console.log(obj);

// let user = {
//   age: 10,
//   name: "John",
//   surname: "Surname",
//   a: false
// };
// console.log(checkHasSurname(user));
// if (!("name" in user)) {
//   console.log("user has not name");
// }
// function checkHasSurname(user) {
//   // return Boolean(user.surname); // 0 undefined null NaN "" false
//   // return user.surname !== undefined; // 0 undefined null NaN "" false
//   // return 'surname' in user;
// }

function generatePropertiesList(obj) {
    if(obj === null || typeof obj !== 'object') {
        console.log('Not an object');
        return;
    }
    let result = "";
    result += "<ul>";
    for (let prop in obj) {
        result += "<li>";
        result += prop;
        result += ": ";
        let value = obj[prop];
        if(typeof value === 'object') {
            value = generatePropertiesList(value);
        }
        result += value;
        result += "</li>";
    }
    result += "</ul>";
    return result;
}
// let user = {
//   age: 40,
//   name: "John",
//   surname: "Surname",
//   child1: {
//     age: 10,
//     child: {
//       name: 'Anyname'
//     }
//   },
//   child2: {
//     age: 11
//   },
// };
// console.log(user.child.age);
// console.log(user['child']['age']);
// {age: 10} {age: 11}
// console.log(user["child 1"])
// for(let i = 0; i < user.childrenCount; i++) { // 0 1
//   console.log(i);
//   console.log(user['child'+(i + 1)])
// }

// let list = generatePropertiesList(user);
// document.write(list);



let user = {
    name: 'John',
    surname: 'Surname',
    age: 20,
    getFullname() {
        return this.name + " " + this.surname;
    },
    print() {
        // console.log("name = " + this.name + ', surname = ' + this.surname + ', age = ' + this.age);
        let result = '';
        for(let prop in this) {
            if(typeof this[prop] !== 'function') {
                result += prop + ' = ' + this[prop] + ',';
            }
        }
        console.log(result);
    }
};
// user.print();
// name = John, surname = Surname, age = 20

function getFullname(obj) {
    return obj.name + " " + obj.surname;
}

// console.log(getFullname(user));
// console.log(user.getFullname());

function printProps(obj) {
    for(let property in obj) {
        console.log(property);
    }
    console.log('----------')
}

let obj2 = {
    name: 1
};
printProps(obj2);
obj2.age = 2;
printProps(obj2);

Object.defineProperty(obj2, 'getFullname', {
    value: function() {
        return this.name;
    },
    enumerable: false,
    configurable: true,
});
console.log(obj2.getFullname())
printProps(obj2);
