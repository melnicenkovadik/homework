// const a = 'string 1';
// const b = 'string 2';
// console.log(a.length);
// const c = a.concat(b);
// const c = a + b;
// const c = `${a}${b}`;
// console.log(c);

// console.log(a[1])
// console.log(a[1] === a.charAt(1))

// console.log(a.toUpperCase().charAt(0).toLowerCase());

//            3          14
const a = 'string 123 string 345';
// console.log(a.startsWith('1str'));
// console.log(a.includes('ing'));
// console.log(a.endsWith('123'));
// начать поиск ing со 2го(3го) символа
// console.log(a.indexOf('ing', 2));
//                     3  6
// findAllOccurrences('string 123 string 345 string 1', 'tt'); // 3 14
// function findAllOccurrences(string, substring) {
//   let index = string.indexOf(substring);
//   // if(index !== -1) { //~index
//   //   console.log('NASHLI');
//   // }
//   while(index > -1) {
//     console.log(index);
//     index = string.indexOf(substring, index + substring.length);
//   }
// }


const str = 'alskfalskdj lka lkhflkf jafsdl fjalksj flkaj ';
// console.log(capitalizeWords(str));

function capitalizeWords(string) {
    let result = '';
    let shouldBeUpperCase = true;
    for (let i = 0; i < string.length; i++) {
        if (shouldBeUpperCase) {
            result += string[i].toUpperCase();
            shouldBeUpperCase = false;
        } else {
            result += string[i];
            if (string[i] === ' ') {
                shouldBeUpperCase = true;
            }
        }
    }
    return result;
}

const z = "alkdfj"; // cde
// console.log(z.repeat(2));
// console.log(z.substr(2, 3))
// console.log(z.substring(-3, 5));
// console.log(z.slice(-3, 5));
// console.log(z.trim());
// let name = prompt('name: ');
// if(name.charCodeAt(0) >= 'A' && name[0] <= 'Z') {
//
// }

//date.getFullYear()
//date.setMonth(4)
//date.setDate(date.getDate() + 7) // add 7 days
//date.getDay() // day of week

function printAllFridaysOfTheMonth() {
    let date = new Date();
    date.setDate(1);
    const friday = 5;
    date.setDate(1 + friday - date.getDay());
    const currentMonth = date.getMonth();
    while(date.getMonth() === currentMonth) {
        console.log(date.getDate());
        date.setDate(date.getDate() + 7);
    }
}

// printAllFridaysOfTheMonth();
// 4
// 11
// 18
// 25

function getDateStr(date) {
    let month = date.getMonth() + 1;
    if(month < 10) {
        month = '0' + month;
    }
    let day = date.getDate();
    if(day < 10) {
        day = '0' + day;
    }
    return `${date.getFullYear()}-${month}-${day}`;
}

const d = new Date();
d.setDate(8);
console.log(getDateStr(d))
