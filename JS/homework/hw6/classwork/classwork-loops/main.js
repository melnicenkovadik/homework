// if(true) {
//   console.log(1);
// }
//
// while(true) {
//   console.log(1);
// }

// let num;
// do {
//   num = +prompt('Number: ');
// } while(isNaN(num));

// let count = 3;
// while(count > 0) {
//   //
//   count--;
// }

// for(let count = 3; count > 0; count--) {
//
// }

// a === 0
// !a

// let str = 'Hello, World!';
// let i = 12;
// console.log(str[str.length - 1]);
// console.log(str.length, str[12]);
// console.log(str.length); // длинна строки
// console.log(str[2]); // 3й символ "l" (отсчет с 0)

// let str = prompt('string:');
// let hasWhitespace = false;
// let iterations = 0;
// for(let i = 0; i < str.length; i++) {
//   if(str[i] === ' ') {
//     hasWhitespace = true;
//     // i = str.length;
//     break;
//   }
//   iterations++;
// }
//
// console.log(hasWhitespace, iterations);

let result = 0, cheatCode = '1';
let prevOperation;

do {
    let num1;
    do {
        num1 = prompt('Number 1'); // "123"
    } while (isNaN(+num1) || num1 === null);

    let userInputOperation;
    let isValidOperation;
    let canUseCheatCode;
    let isCheatCode;
    do {
        userInputOperation = prompt('Operation:'); // "1"

        isValidOperation = userInputOperation === '*'
            || userInputOperation === '/'
            || userInputOperation === '+'
            || userInputOperation === '-';
        canUseCheatCode = Boolean(prevOperation);
        isCheatCode = userInputOperation === cheatCode;
    } while (!(isValidOperation || (canUseCheatCode && isCheatCode)));

    let operation;
    if(userInputOperation === cheatCode) {
        operation = prevOperation;
    }
    else {
        operation = userInputOperation;
    }

    let num2;
    do {
        num2 = prompt('Number 2'); // "2"
    } while (isNaN(+num2) || num2 === null);

    switch (operation) {
        case '*':
            result = num1 * num2;
            break;
        case '/':
            result = num1 / num2;
            break;
        case '+':
            result = +num1 + +num2;
            break;
        case '-':
            result = num1 - num2;
            break;
    }

    prevOperation = operation;
} while (confirm("Result: " + result + ".\nEshe raz?"));
