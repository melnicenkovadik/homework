const input = document.querySelector('#priceInpt');
const result = document.querySelector('.price');

input.onblur = function () {
    const span = document.createElement('span');
    const deleteBtn = document.createElement('button');
    const pnh = document.createElement('h1');
    span.innerText = `Текущая цена: ${input.value}`;
    span.classList.add('span4ik');


    deleteBtn.innerText = 'x';
    deleteBtn.classList.add('delete-btn');
    deleteBtn.addEventListener('click', () => {
        span.remove();
        input.value = 0;
    });
    span.appendChild(pnh);
    span.appendChild(deleteBtn);
    result.appendChild(span);

    // while (input.innerText <= '0') {
    //     input.style.border = '3px';
    //     input.style.borderColor = 'red';
    //     pnh.innerText = 'Please enter correct price';
    //     span.style.color = 'red';
    // }
    if (pnh.value > 0) {
        input.style.border = '3px';
        input.style.borderColor = 'green';
        pnh.innerText = 'Please enter correct price';
        pnh.style.border = 'green';
        span.style.color = 'green';
    }


};
