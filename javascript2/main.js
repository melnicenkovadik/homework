// let count = 0;
// let lastTime = Date.now();
// setInterval(func, 1000);
// function func() {
//     const now = Date.now();
// console.log(++count, now - lastTime);
// lastTime = now;
// }

const clocks = document.querySelector('.clocks');
const clocks2 = document.querySelector('.clocks2');
const fps = 60; // frames per second
const deltaTime = 1000 / fps; // 16.666667
let time = new Date();
setInterval(printTime, deltaTime);
setInterval(printTime2, deltaTime);
function printTime() {
    time.setMilliseconds(time.getMilliseconds() + deltaTime);
    clocks.textContent = time.toTimeString().slice(0,8) + '.' + time.getMilliseconds();
}
function printTime2() {
    const date = new Date();
    clocks2.textContent = date.toTimeString().slice(0,8) + '.' + date.getMilliseconds();
}