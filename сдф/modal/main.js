function openModal(text) {
    const modal = document.createElement('div');
    modal.classList.add('modal');
    modal.innerText = text;
    document.body.append(modal);
    document.addEventListener('click', onBodyClick);
}
function onBodyClick(event) {
    if (!event.target.closest('.modal')) {
        modal.remove();
        document.removeEventListener('click', onBodyClick);
    }
}
const form = document.querySelector('form');
form.addEventListener('submit', onSubmit);
function onSubmit(event) {
    event.preventDefault();
    // console.log(form.elements.email.value);
    const data = new FormData(form);
    openModal(data.get('text'));
}